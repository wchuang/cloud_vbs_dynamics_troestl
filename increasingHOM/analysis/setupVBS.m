function setupVBS

setupVBSstd;

% see setupVBSstd.m for the default parameter settings.
% The following parameters override the ones setupVBSstd.m.

global parms

parms.timeStart = 0; % in minutes
parms.timeEnd = 80; % in minutes

parms.Lprecursor = 0; % current parameter to test
parms.precTimeCoeff = 0.00045*2.1;%parms.transEff;

%parms.DK = 3.75; 

% parameter not yet used
parms.T = 278; % [K]

% 1,2,3, in increasing verbosity; 
% can utilize version numbers (e.g. 1.01,3.2,etc.)
parms.plotVerbosity = 1.018;

% to fit the plot size
parms.Dpmax = 30*.58;                  % Max dp for linear GR figure
parms.GRmax = 15*2.1;                   % Max GR for GR figures

%% with new parameters 06/08/2016, August 6
parms.precTimeCoeff = 0.00056;%parms.transEff;
parms.transEff = 1.3; % transmission detection of HOM concentrations
          % -8       -7       -6       -5        -4       -3      -2       -1      0%
parms.y = [.012;   .0032;   .0035;   .0065;   .00938;  .000627;  .0155;  0.0086;  .017];
parms.y = [.013;   .0033;   .0034;   .0058;   .0072;   .0112;    .0116;  0.0100;  .0158];    
sum(parms.y)
parms.chargeEfficiency = [1;  1;  1;  1;  1/8;  1/8;  1/10; 1/10; eps];
parms.y = parms.y ./ parms.chargeEfficiency;
parms.y(end) = 0;
parms.y(end-1) = 0;
parms.y = parms.y * parms.transEff;
sumYield = sum(parms.y)
parms.Morg = [478; 426; 395; 386; 314; 275; 274; 269; 246];
%%
%% Oligomerization? *** only works with polyisperse = 0
parms.oligomerization = 1; % 0 and 1    
%% *** must take out later. Testing for oligomerization
if parms.oligomerization == 1
    parms.y = [.013;   .0033;   .0034;   .0058;   .0072;     .0112;       .0116;   0;    0.2]; %original yields from ELVOC_properties.xls calculations   
    parms.Co = [10.^(-8:-1), 1e1]';
    sum(parms.y)
    parms.Morg = [478; 426; 395; 386; 314; 275; 274; 269; 175];
end
pause