function [Dm_obs, t_obs] = readDiameters

% open the ELVOC mass distribution file
fid = fopen('./data/Measurement_increasing.txt');
C = textscan(fid,'%f %f/%f/%f %f:%f:%f','HeaderLines',1);
fclose(fid);

% pull out the two vectors from the input file
Dm_obs = C{1};
day_obs = C{2};
month_obs = C{3};
year_obs = C{4};
hr_obs = C{5};
min_obs = C{6};
sec_obs = C{7};

% Time is in minutes from start, assume no month or year change
times = (day_obs*24 + hr_obs)*60 + min_obs +sec_obs/60;
t_obs = times - times(1);

