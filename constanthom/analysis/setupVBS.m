function setupVBS
clc
clear
setupVBSstd;

% see setupVBSstd.m for the default parameter settings.
% The following parameters override the ones setupVBSstd.m.

global parms

parms.timeStart = -60; % in minutes (-60)
parms.timeEnd = 270; % in minutes (270)

parms.Lprecursor = 0.0101; % current parameter to test
parms.precTimeCoeff = 0;

%parms.DK = 4.5;

% parameter not yet used
parms.T = 278; % [K]

% 1,2,3, in increasing verbosity;
% can utilize version numbers (e.g. 1.01,3.2,etc.)
parms.plotVerbosity = 1.0050; 

%% new distributions to test, 06082016
parms.transEff = 1.3; % transmission detection of HOM concentrations
          % -8       -7       -6       -5        -4         -3       -2       -1       0%
parms.y = [.012;   .0032;   .0035;   .0065;   .00938;    .000627;  .0155;   0.0086;   .017];
parms.y = [.013;   .0033;   .0034;   .0058;   .0072;     .0112;    .0116;   0.0100;   .0158];    
sum(parms.y)
parms.chargeEfficiency = [1;  1;  1;  1;  1/8;  1/8;  1/10; 1/10; eps];
parms.y = parms.y ./ parms.chargeEfficiency;
parms.y(end) = 0;
parms.y(end-1) = 0;
parms.y = parms.y * parms.transEff;
sumYield = sum(parms.y)

%% Oligomerization? *** only works with polyisperse = 0
parms.oligomerization = 1; % 0 and 1    
%% *** must take out later. Testing for oligomerization
if parms.oligomerization == 1
    parms.y = [.013;   .0033;   .0034;   .0058;   .0072;     .0112;       .0116;   .000;    0.2]; %original yields from ELVOC_properties.xls calculations   
    sum(parms.y)
    parms.Co = [10.^(-8:-1), 1e1]';
    sum(parms.y)
    parms.Morg = [478; 426; 395; 386; 314; 275; 274; 269; 175];
end
%%
pause