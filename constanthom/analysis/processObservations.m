function [Cst,y] = processObservations(cStarMult,yELVOC,chargeE)

if nargin < 2, 
    yELVOC = 0.12; % The assumed ELVOC mass yield
end

if nargin < 1, 
    cStarMult = 1; % scaling for uncertainty in C*
end

if nargin < 3,
    chargeE = 1;
end


% open the ELVOC mass distribution file
fid = fopen('./data/GasDistribution.txt');
C = textscan(fid,'%f%f','HeaderLines',1);
fclose(fid);

% pull out the two vectors from the input file
Cstar = C{1};
Conc = C{2};

% turn them around
decades = log10(cStarMult);
Cstar = Cstar(end:-1:1) + decades;
Conc = Conc(end:-1:1);

% this is amazingly clunky, but if we are making the stuff less volatile
% we need to make sure we have bins up to at least 100 ug/m3
if decades < 0
    bigCstar = Cstar(end);
    for n = (decades:-1)
        Cstar = [Cstar; bigCstar - n];
        Conc = [Conc; 0];
    end
end

% now make a figure
hold off
figure 
setFigureParameters([6.5,3],'log_{10} C^* (\mu g m^{-3})','C (\mu g m^{-3})');

bar(Cstar,Conc,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('C (\mu g m^{-3})');

saveas(gcf,'./figs/C_CLOUD.pdf','pdf');
%pause


% Now figure out the mass yields, assuming that all C* <= 0 are "ELVOC"
sel = Cstar <= 0;
CELVOC = sum(Conc(sel));

AMF = yELVOC/CELVOC * Conc;

bar(Cstar,AMF,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('mass yield');

saveas(gcf,'./figs/AMF_CLOUD.pdf','pdf');
%pause

% Now collect this into a VBS ending at 1-e5 but with a 1e-8 SLVOC bin
% SLVOC = super low volatility 
sel = Cstar <= -8;
YSLVOC = sum(AMF(sel));

% The rest of the VBS will be from 1e-7 to 1
sel = (Cstar >= -7) & (Cstar < 1);
AMFbase = AMF(sel);
Cstarbase = Cstar(sel);


AMFVBS = [YSLVOC; AMFbase];
CstarVBS = [-8; Cstarbase];

% Now adjust by charging efficiency
AMFVBS = AMFVBS ./ chargeE;


% OOPSIE -- WAYNE CHECK THIS 28 June 2015
AMFVBS(end) = 0.10;   % yield in 1 ug/m3 bin

AMFAPiTOF = AMFVBS .* chargeE;

% I think the yields are x2 too high for some reason
% but this is a hack
br = bar(CstarVBS,[AMFAPiTOF,AMFVBS-AMFAPiTOF]/2,'stack');

set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
set(br(2),'FaceColor',[.75 1 .75]);
set(br(2),'LineWidth',2);

xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('product distribution');
set(gca,'YTickLabel',[]);
set(gca,'YTick',[]);
%ylabel('mass yields');

saveas(gcf,'./figs/VBS_CLOUD.pdf','pdf');

%% background shading

grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 5.5, .06],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 4, .06],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, .06],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');        

t1 = text((-8.5-4.5)/2, 0.063, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, 0.063, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.75, 0.063, 'SVOC','HorizontalAlignment','center','FontSize',16);

set(gca,'layer','top')

saveas(gcf,'./figs/VBS_CLOUD_color.pdf','pdf');

ELVOC2 = rectangle('Position',[-10, 0, 6.5, .06],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel,'Linestyle','none');
uistack(ELVOC2, 'bottom');
uistack(ELVOC2, 'up');
uistack(ELVOC2, 'up');

delete(t2);
t2 = text(-2, 0.063, 'LVOC','HorizontalAlignment','center','FontSize',16);

set(gca,'layer','top')
saveas(gcf,'./figs/VBS_CLOUD_color2.pdf','pdf');

%%
% set up return parameters
Cst = 10.^CstarVBS;
y = AMFVBS;


% % continue with some more stuff
% now show the cumulative concentrations
bar(Cstar,cumsum(Conc),'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('\Sigma C (\mu g m^{-3})');

saveas(gcf,'./figs/sumC_CLOUD.pdf','pdf');
% pause

% now show the saturation ratios

% include the VBS SOA
Conc(end-0) = 0.02 * CELVOC/yELVOC;


sumSrat = cumsum(Conc)./(10.^Cstar);
bar(Cstar,log10(sumSrat),'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('log_{10} S^{eff} = (\Sigma C) / C_i^*');
ylim([-5 +5]);

saveas(gcf,'./figs/S_CLOUD.pdf','pdf');


% now show condensing fraction
fCond = 1./(1 + 1./sumSrat);
bar(Cstar,fCond,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('Condensing fraction');

saveas(gcf,'./figs/fCond_CLOUD.pdf','pdf');


% now show condensing fraction
fELVOC = cumsum(Conc.* fCond) / CELVOC;
bar(Cstar,fELVOC,'g');
ylim([0,1]);
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('Fraction ELVOC condensed');

saveas(gcf,'./figs/fELVOC_CLOUD.pdf','pdf');
