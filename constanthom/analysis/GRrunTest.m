clc
clear
global parms

setupVBS;  

physicalConstants;
diameters = [1.1; 3.2; 5; 15; 50];  
parms.timeEnd = 10; % Looking at GR, HOM at start
parms.Np = 1e3;

i = 10.^(-1:0.1:2)';
precursor = parms.Lprecursor;
HOMs = zeros(size(i,1), size(diameters,1)); % initialization
GRtots = zeros(size(i,1), size(diameters,1)); % initialization
Dps = zeros(size(i,1), size(diameters,1)); % initialization
    
for k = 1:size(diameters)
    parms.DpSeed = diameters(k);
    
    for j = 1:size(i)
        parms.Lprecursor = i(j) * precursor;
        [t_prod Cprod] = simpleVBSDynamics;
        Cv_prod = Cprod(:,(1:9));     % vapors during dilution
        Cs_prod = Cprod(:,(10:18));   % suspended particles during dilution
        Cseed_prod = Cprod(:,19);     % suspended seed mass during dilution
        Np_prod = Cprod(:,20);        % suspended particle number during dilution

        COA_prod = sum(Cs_prod,2);    % sum of each row
        Cvap_prod = sum(Cv_prod,2);   % sum of each row for vapors


        C_vapELVOCprod = sum(bsxfun(@times,Cv_prod,parms.chargeEfficiency'),2); % sum of all ELVOCs
        C_vapELVOCmlcprod = C_vapELVOCprod/250*6e23/1e12; % is called in vaporPlot.m

        calcForPlots;

        HOMs(j,k) = Nv_tot(10);
        GRtots(j,k) = GR_tot(10);
        Dps(j,k) = Dp_prod(10);
    end
end

paperSize = [9 5];
figure
pl = plot(HOMs(:,1), GRtots(:,1), 'Color', 'k');
set(pl,'LineWidth',3);
hold on
pl = plot(HOMs(:,2), GRtots(:,2), 'Color', 'r');
set(pl,'LineWidth',3);
xLabelStr = 'HOM Concentration [cm^{-3}]';
yLabelStr = 'Growth Rate [nm hr^{-1}]';
xlim([2e6 1e9]);
ylim([1e-1 2e2]);
set(gca,'XScale','log');
set(gca,'YScale','log');
setFigureParameters(paperSize,xLabelStr,yLabelStr);
legend({'1.1 nm','3.2 nm'},'Location','SouthEast');
saveas(gcf,['./figs','/figc'],'fig');
saveas(gcf,['figs','/figc.pdf'],'pdf');

figure
pl = plot(HOMs(:,3), GRtots(:,3), 'Color', 'r');
set(pl,'LineWidth',2);
hold on
pl = plot(HOMs(:,4), GRtots(:,4), 'Color', 'b');
set(pl,'LineWidth',2);
hold on
pl = plot(HOMs(:,5), GRtots(:,5), 'Color', 'g');
set(pl,'LineWidth',2);
xLabelStr = 'HOM Concentration [cm^{-3}]';
yLabelStr = 'Growth Rate [nm hr^{-1}]';
xlim([2e6 1e9]);
ylim([1e-1 2e2]);
set(gca,'XScale','log');
set(gca,'YScale','log');
setFigureParameters(paperSize,xLabelStr,yLabelStr);
legend({'5 nm','15 nm','50 nm'},'Location','SouthEast');
saveas(gcf,['./figs','/figd'],'fig');
saveas(gcf,['figs','/figd.pdf'],'pdf');
pause