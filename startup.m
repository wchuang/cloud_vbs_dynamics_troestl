
disp('Local startup stuff')

%%%%%%%%%%
% Standard path construction
% All analysis scripts in the donahue group are designed to be
% run within a standardized directory structure.

% This is where all of the CMU packages live.
% Change it as necessary to reflect where you put them!
scriptDirCMU = 'C:/Users/waynechu/Documents/MATLAB/scripts';

if exist([scriptDirCMU  '/whereTheyAre']) == 7
  addpath([scriptDirCMU, '/whereTheyAre']);
end
if exist('/Users/nmd/Data/scripts/matlab/whereTheyAre') == 7
    addpath /Users/nmd/Dropbox/Software/scripts/matlab/whereTheyAre;
end
whereThingsAreCMU

