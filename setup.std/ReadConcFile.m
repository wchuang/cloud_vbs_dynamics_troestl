clc
clear

%%
C = tdfread('/data/MassCompounds.txt');
Cstar = C.Cmod;
Comp = C.Composition;
Conc = C.Concentration_cm_3_;
MW   = C.Exactmass_Th_;

Conc(isnan(Conc)) = 0;

Cdata = [Cstar Conc MW];
Csorted = sortrows(Cdata, [-1 3]);

AvgMW = zeros(10,1);
NO3 = zeros(length(Comp),1);
HNO3 = zeros(length(Comp),1);

for i=1:length(Comp)
    NO3(i,1) = ~isempty((strfind(Comp(i,:), 'NO3') > 1)) * 62; % account for MW of NO3
    HNO3(i,1) = ~isempty((strfind(Comp(i,:), 'HNO3')>1)) * 63; % account for MW of HNO3
end

MW = MW - NO3 - HNO3; % subtract the MW of nitrate ions

for i = -7:1
    j = Cstar < i+0.5 & Cstar > i-0.5; % True for compounds that satisfy conditions
    mass_tot_i = sum(j .* Conc);
    AvgMW_i = sum(j .* Conc .* MW) / mass_tot_i;
    
    AvgMW(i+9,1) = AvgMW_i;
end

j = Cstar < -7.5;
mass_tot_i = sum(j .* Conc);
AvgMW_i = sum(j .* Conc .* MW) /mass_tot_i;
AvgMW(1,1) = AvgMW_i;
AvgMW = flipud(AvgMW);