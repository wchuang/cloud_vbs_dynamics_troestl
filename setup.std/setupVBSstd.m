function setupVBSstd

% setup VBS has values for calcs with a global parameter structure

global parms

% This is a volatility distribution tailored to CLOUD, using
% an existing 8-bin dynamical code.  One is really, really low
% volatility to more or less seed condensation
parms.Co = [10.^(-8:0)]';           % volatility of products
parms.selHOM = parms.Co < 1;                % selector for HOMs
parms.relTol = 1e-10;                       % relative tolerance for solver
parms.absTol = 1e-13;                       % absolute tolerance for solver

% yields of products
% this one is not currently used. It is pulled from processObservations
parms.y = [.01; .01; .02; .02; .02; .02; .02; .05; .10];

parms.CstarShift = 1e-1; % shift of the volatility bins
%parms.chargeEfficiency = [1; 1; 1; 1; .25; .2; .2; .2; eps];
parms.chargeEfficiency = [1;  1;  1;  1;  .50;  .25;  .1; .1; eps]; % v1.2
parms.chargeEfficiency = [1;  1;  1;  1;  .50;  .25;  .125; .1; eps]; % new
%parms.chargeEfficiency = [1;  1;  1; .4;  .30;  .20;  .20;   .2; eps]; % for DK=4.5
%parms.chargeEfficiency = [1; 1; 1; 1; .5; .4; .3; .1; eps]; % for DK=0
%parms.chargeEfficiency = [1;  1;  1;  1;  1;  1;  1; 1; eps]; % new

parms.yieldELVOC = 0.12;

% We have constraints from CLOUD so we will pull the VBS from there
[parms.Co, parms.y ] = ...
    processObservations(parms.CstarShift,parms.yieldELVOC,parms.chargeEfficiency);

[parms.Dm_obs, parms.t_obs] = readDiameters;

parms.vorg = 44;                  % m/s condensation velocity

% Molecular weight g/mole of VBS, total guess right now
parms.Morg = [460; 400; 350; 302; 296; 280; 264; 248; 232];
%parms.Morg = [455; 397; 372; 311; 268; 347; 271; 248; 248]; %calculated
%from ReadConcFile.m

parms.Dorg = 0.9;               % Diameter of a HOM monomer in nm

parms.alphaOrg = 1;               % mass accomodation coefficient
parms.lambda = 80;                % mean free path should calculate
parms.rhoOrg = 1.3e3;             % kg/m3

parms.DpSeed = 0.9;               % nm so mult by 1e-9 to get nm
    % Mobility diameter is later calculated by Dpseed + interactSeed
parms.interactSeed = 1;%.75;           % interaction term between seeds and org (.75)

parms.rhoSeed = 1.3e3;            % kg/m3 -- assuming seed is organic too...
parms.DK = 3.75;                   % Decadal Kelvin diameter in nm

parms.Np = 1.0e3;                 % cm-3 so mult by 1e6 to get m-3

% time span of condensation
parms.timeStart = -60; % in minutes
parms.timeEnd = 270; % in minutes

% We wind up with about 0.01 ug/m3 HOMs, and they have a mass yield
% somewhere close to 0.1, meaning that you need to oxidize 0.1 ug/m3 of
% a-pinene to get 0.01 ug/m3 of HOMs. With a wall loss of order 0.1/min
% (lifetime of 10 min), the oxidation rate is about 0.01 ug/m3-min
parms.transEff = 1.3; % Empirical to get HOMs
parms.Lprecursor = (0.008*1.05);        % The precursor loss rate for fcn ug/m3-min
                             
parms.precTimeCoeff = 0;           % How precursor loss rate varies with time    

parms.kw = .1 * ones(size(parms.y)); % wall loss.

parms.kflow = 1/(300*60); % 3 hour timescale for CSTR timescale

parms.maxY = 0.1;                  % A scale value for maximum COA to plot
parms.maxYPart = 1e-2;             % A scale value for maximum COA to plot
parms.maxYield = 0.05;             % max yield for odum plot

parms.yLimAct = [1e-5,1e7];        % Note this needs to change with Lprec

parms.OASeedY = [.1,1e3];
parms.DiameterY = [1,3];
parms.CSY = [1e-5,.1];

parms.Dpmax = 30;                  % Max dp for linear GR figure
parms.GRmax = 15;                   % Max GR for GR figures

parms.plotVerbosity = 1; % levels 1,2,3