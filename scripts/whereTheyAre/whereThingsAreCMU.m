% First (lowest precedence) is analysis.std 1 dir up
if exist('../analysis.std') == 7
  if exist('../analysis.std/matlab') == 7
        addpath ../analysis.std/matlab
    end
    addpath ../analysis.std
end
% Next is analysis.std in this dir
if exist('./analysis.std') == 7
    if exist('./analysis.std/matlab') == 7
        addpath ./analysis.std/matlab
    end
    addpath ./analysis.std
end
% Followed by analysis in this dir (this overrides the others)
if exist('./analysis') == 7
    if exist('./analysis/matlab') == 7
        addpath ./analysis/matlab
    end
    addpath ./analysis
end

if exist('./results.std') == 7
    addpath ./results.std
end
if exist('./results') == 7
    addpath ./results
end
% Highest precidence is anything in overrides, meant to be temporary
if exist('./overrides') == 7
    addpath ./overrides
end

% add path for parameter setup folder
if exist('../setup.std') == 7
    addpath ../setup.std
end

if exist('./setup.std') == 7
    addpath ./setup.std
end



startupAnalysis;
