v1.0

First open the startup.m file and change the path to point to whereThingsAreCMU.m then copy the file to the folder(s) where you would start Matlab (increasingHOM or constantHOM folders).
The files will direct Matlab to add paths to read from and write to.
Start Matlab from the folder in which you will make the run (e.g. increasingHOM).
Matlab will call startup.m, then whereThingsAreCMU.

Standard parameters are located in setupVBSstd, and specific parameters that override the standard parameters are located in setupVBS.

The dependency of the code is:
	RunMe
		setupVBS
			setupVBSstd
				processObservations
				readDiameters
		simpleVBSDynamics
			physicalConstants
			vaporBuildup
				@dynVBSbuildup
			@dynVBS
			vaporPlot
			particlePlot % left for archiving purposes. Currently unused.
			totalPlot    % left for archiving purposes. Currently unused.
			mainPlots
			summaryPlots % summaryPlots is an older form of mainPlots. Currently unused.

			
Additional files:
	ReadConcFile.m - located in the setup.std folder
		This reads Jasmin's data on the molecular weight of products and calculates a mass-concentration-weighted average MW for each volatility bin in the VBS.
		Run the file after starting Matlab from a run folder.
	ReadAPO3File - located in the setup.std folder
		This reads Jasmin's data on HOM concentrations.