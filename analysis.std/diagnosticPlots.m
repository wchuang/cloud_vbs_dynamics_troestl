global parms

% Plot the suspended organic concentration
yVarName = 'Cs_prod';
plotVBSVec;
pl = eval(['semilogy(',xVarName,',Cpart,''k--'')']);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,xLabelStr,'C^s (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/Cs_v_',fileSuffix,'.pdf'],'pdf');

% Plot the equilibrium concentration
yVarName = 'Ceq';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Equilibrium Concentration (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/Ceq_v_',fileSuffix,'.pdf'],'pdf');

% Plot the vapor concentration
yVarName = 'Cv_prod';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Vapor Concentration (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/Cv_v_',fileSuffix,'.pdf'],'pdf');

% Plot the HOM concentration in number concentration (only low C*)
yVarName = 'Nv';
plotVBSVec;
pl = eval(['semilogy(',xVarName,',Nv_tot,''k--'')']);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,xLabelStr,'HOM (cm^{-3})');
saveas(gcf,['./figs',verbos,'/Nv_v_',fileSuffix,'.pdf'],'pdf');

% Plot the driving force
yVarName = 'C_drive';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Condensation Driving Force (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/C_drive_v_',fileSuffix,'.pdf'],'pdf');

% Plot the condensation flux
yVarName = 'C_flux';
plotVBSVec;
pl = eval(['semilogy(',xVarName,',C_flux_tot,''k--'')']);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,xLabelStr,'Flux per area concentration (\mu g m^3/m^3-s-nm^2)');
saveas(gcf,['./figs',verbos,'/flux_v_',fileSuffix,'.pdf'],'pdf');

% Plot the condensed phase activity
yVarName = 'as_prime';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Condensed phase activity');
ylim(parms.yLimAct);
saveas(gcf,['./figs',verbos,'/activity_v_',fileSuffix,'.pdf'],'pdf');

% Plot the condensed phase mass fraction
yVarName = 'as';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Condensed phase mass fraction');
saveas(gcf,['./figs',verbos,'/massFraction_v_',fileSuffix,'.pdf'],'pdf');

% Plot the vapor activity
yVarName = 'Sv';
plotVBSVec;
ylim(parms.yLimAct);
setFigureParameters(paperSize,xLabelStr,'Vapor Saturation Ratio');
saveas(gcf,['./figs',verbos,'/S_v_',fileSuffix,'.pdf'],'pdf');

% Plot the excess saturation
yVarName = 'XS_sat';
plotVBSVec;
ylim(parms.yLimAct);
setFigureParameters(paperSize,xLabelStr,'Excess saturation');
saveas(gcf,['./figs',verbos,'/excessSaturation_v_',fileSuffix,'.pdf'],'pdf');


return 

% Plot the equilibrium concentration over a flat surface
yVarName = 'Ceq_flat';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Equilibrium Concentration Flat (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/Ceq_flat_v_',fileSuffix,'.pdf'],'pdf');

% Plot the flat driving force
yVarName = 'C_drive_flat';
plotVBSVec;
setFigureParameters(paperSize,xLabelStr,'Flat Driving Force (\mu g /m^3)');
saveas(gcf,['./figs',verbos,'/C_drive_flat_v_',fileSuffix,'.pdf'],'pdf');

%  Plot flat excess saturation
yVarName = 'XS_sat_flat';
plotVBSVec;
ylim(parms.yLimAct);
setFigureParameters(paperSize,xLabelStr,'Excess saturation (flat)');
saveas(gcf,['./figs',verbos,'/excessSaturation_flat_v_',fileSuffix,'.pdf'],'pdf');
