% Produces the main vapor phase graphs

verbos = num2str(parms.plotVerbosity);
% tests if the folder for this 'verbos' version exists. if not, make folder
if exist(['./figs' verbos], 'dir') ~= 7
    mkdir(['./figs' verbos])
end

calcForPlots;

%% total vapor concentration vs. time
figure
if parms.timeStart < 0
    xVarName = 't_init';
    yVarName = 'Cv_init';
    fStr = ['semilogy(',xVarName,',',yVarName,'(:,1),''g-'')'];
    pl = eval(fStr);
    set(pl,'Color',[0,1/10,0]);
    set(pl,'LineWidth',1.5);
    hold on
    for v = (2:9)
        pl = eval(['semilogy(',xVarName,',',yVarName,'(:,v),''g-'')']);
        set(pl,'Color',[0,v/10,0]);
        set(pl,'LineWidth',1.5);
    end
end

xVarName = 't_prod';
yVarName = 'Cv_prod';
fStr = ['semilogy(',xVarName,',',yVarName,'(:,1),''g-'')'];
pl = eval(fStr);
set(pl,'Color',[0,1/10,0]);
set(pl,'LineWidth',1.5);
hold on
for v = (2:9)
    pl = eval(['semilogy(',xVarName,',',yVarName,'(:,v),''g-'')']);
    set(pl,'Color',[0,v/10,0]);
    set(pl,'LineWidth',1.5);
end

setFigureParameters(paperSize,'time (min)','Vapor Concentration (\mu g m^{-3})');
saveas(gcf,['figs',verbos,'/Cvap_log' '.pdf'],'pdf');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HOM concentration over time
figure
% HOM concentration during the buildup phase
if parms.timeStart < 0
    pl = plot(t_init, C_vapELVOCmlcinit, 'g-');
    set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',5);
    hold on;
end

% Plot reaction phase HOM
pl = plot(t_prod, C_vapELVOCmlcprod, 'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',5);
setFigureParameters(paperSize,'time (min)','HOM (cm^{-3})');
saveas(gcf, ['figs',verbos,'/HOM' '.pdf'], 'pdf');