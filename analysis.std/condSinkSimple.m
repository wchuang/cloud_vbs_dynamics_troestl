function [ kc, Dp, freqPerArea ] = condSinkSimple(Np, COA, Cseed)
%condSinkSimple calculate condensation sink also returns particle diameter
%   Np is number concentration of particles     1/cm3
%   COA is organic aerosol concentration        ug/m3
%   Cseed is seed concentration                 ug/m3
%   speed is the mean molecular speed
%  size notations in the comments denote the size of the arrays after dynamic runs are finished.
%  It is called from the plotting files.

global parms

physicalConstants;

% Figure out the particle volume in m3
% Assume monodispersed and uniform concentration
Vp = (Cseed/parms.rhoSeed + COA/parms.rhoOrg)./((Np*cm3tom3) * kgm3tougm3); %size(length(t),1)

% Now assume a sphere and volume mixing of seed and OA to figure out Dp
Dp = ((6/pi)*Vp).^(1/3) / nm2m; %size(length(t),1)

% And the particle mass in amu
Mp = (Cseed + COA)./((Np*cm3tom3))* ugtoamu; % size(length(t),1)

% Figure out the reduced mass of the collision (vapor and particle)

% The reduced mass includes (monodisperse) particle mass
mu = bsxfun(@times, parms.Morg', Mp) ./ bsxfun(@plus, parms.Morg', Mp); % size(length(t), length(C*))

% In this version the speed is the center of mass collision speed 
speed = meanSpeed(mu, parms.T)/4; % size(length(t), length(C*))

% Calc cond sink, comes up in 1/s so convert to 1/min
beta = FuchsSutugun(Dp,parms.lambda,parms.alphaOrg); % size(length(t), 1)

% Molecular enhancement (convert cross section to surface)
e_ip = (Dp+parms.Dorg).^2 ./ Dp.^2;

% The units of the frequency per area are 
%   1/time * area/volume = 1/time-length
%   here it is 1/(min-m)
% units don't work out...
% This is actually the deposition rate coefficient, with units of [length/time]
freqPerArea = bsxfun(@times, speed, beta) * sec2min; % size(length(t), length(C*))
freqPerArea = bsxfun(@times, freqPerArea, e_ip); % 

% Calculate the aerosol particle surface area
surfArea = (pi*(Dp*nm2m).^2);

% Calc cond sink, comes up in 1/s so convert to 1/min
% addition of time variable requires a transposition for the dynamic
% portion of the code ( dynVBS has kc transposed when
% calculating phic_p)
kc = bsxfun(@times, freqPerArea, surfArea .* (Np*cm3tom3) );

end

