function s = meanSpeed(Mi,T)

% s= meanSpeed(Mi,T)
%
% Mean molecular speed given 
% Mi        = molar weight (g/mole)
% T [300]   = temperature (K)

if nargin < 2, T = 300; end;

physicalConstants;

s = sqrt(8000 * R .* T ./ (pi .* Mi) );