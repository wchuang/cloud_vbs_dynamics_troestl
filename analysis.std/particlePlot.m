
% total particle concentration vs. time
if parms.timeStart < 0
    xVarName = 't_init';
    yVarName = 'Cs_init';
    plotCmd = 'semilogy';
    plotVBSVec;
    hold on
end

xVarName = 't_prod';
yVarName = 'Cs_prod';
fStr = ['semilogy(',xVarName,',',yVarName,'(:,1),''g-'')'];
pl = eval(fStr);
set(pl,'Color',[0,1/10,0]);
set(pl,'LineWidth',1.5);
hold on
for v = (2:9)
    pl = eval(['semilogy(',xVarName,',',yVarName,'(:,v),''g-'')']);
    set(pl,'Color',[0,v/10,0]);
    set(pl,'LineWidth',1.5);
end



setFigureParameters(paperSize,'time (min)','C^{particle} (\mu g m^{-3})');
ylim([0,1].*parms.maxYPart);
xlim([0,100]);

a1 = gca;

a2 = axes;
axis(lims);
setFigureParameters(paperSize,'','');

set(a2,'Color','none');
set(a2,'YAxisLocation','right');
%ylim([0,1].*parms.maxYPart/parms.dilRatio);


saveas(gcf,'figs/testC_Cpart_lin.pdf','pdf');

set(a1,'YScale','log');
%set(a2,'YScale','log');

axes(a1);
ylim([1e-4,1].*parms.maxYPart);

axes(a2);
%ylim([1e-4,1].*parms.maxYPart/parms.dilRatio);

saveas(gcf,'figs/testC_Cpart_log.pdf','pdf');

