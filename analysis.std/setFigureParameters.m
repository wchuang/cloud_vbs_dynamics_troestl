function setFigureParameters(paperSize,xStr,yStr)

set(gca,'FontSize',14);
xlabel(xStr);
set(get(gca,'Xlabel'),'FontSize',18);
ylabel(yStr);
set(get(gca,'Ylabel'),'FontSize',18);
set(gca,'LineWidth',1.5);

warning('off','MATLAB:hg:ColorSpec_None')
%%set(gca,'Color','none');
set(gca,'TickLength',[0.025,0.01]);
set(gca,'LineWidth',1.5);


% Position the figure and set the size so the pdf is well cropped
set(gcf,'Units','inches');
pos = get(gcf,'Position');
set(gcf,'Position',[[pos(1) pos(2)]  paperSize]);
set(gcf,'PaperPosition',[[0 0] paperSize]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', paperSize);
%sset(gcf,'Color','none');


