function DK10 = KelvinDiameter(sigmai, Mi, rhoi, T)

physicalConstants

DK10 = log10(exp(1)) * 4 * sigmai .* (Mi*g2kg) ./ (R * T * rhoi) / nm2m;