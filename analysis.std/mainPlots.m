verbos = num2str(parms.plotVerbosity);

if exist(['./figs' verbos], 'dir') ~= 7
    mkdir(['./figs' verbos])
end

calcForPlots;

if parms.plotVerbosity >= 1
    
    xVarName = 't_prod';
    xLabelStr = 'time (min)';

    %% Plot the HOM concentration in number concentration (only low C*) vs time
    plotCmd = 'plot';
    yVarName = 'Nv';
    fileSuffix = 'lin_t';
    plotVBSVec;
    pl = eval(['plot(',xVarName,'(plotSel),Nv_tot(plotSel),''k--'')']);
    set(pl,'LineWidth',1.5);
    setFigureParameters(paperSize,xLabelStr,'HOM (cm^{-3})');
    saveas(gcf,['./figs',verbos,'/Nv_v_',fileSuffix,'.pdf'],'pdf');

    %% Diameter plot vs time

    figure
    pl = semilogy(t_prod,Dp_prod+0.3,'g-'); % +0.3 nm for mobility diameter
    set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',1.5);

    hold

    % Here are the observations -- better have been read!
    pl2 = plot(parms.t_obs,parms.Dm_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);

    uistack(pl,'top');

    setFigureParameters(paperSize,'time (min)','Particle Diameter (nm)');
    saveas(gcf,['figs',verbos,'/diameter_v_t_log.pdf'],'pdf');

    set(gca,'YScale','lin');
    saveas(gcf,['./figs',verbos,'/diameter_v_t_lin'],'fig');
    saveas(gcf,['figs',verbos,'/diameter_v_t_lin.pdf'],'pdf');

    %% Stacked plot, growth rate vs Dp
    % here assigns log C* = -8 to -5 as ELVOC
    xVarName = 'Dp_prod';
    yVarName = 'GR';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp_stacked';
    plotCmd = 'area';

    figure
    pl = area(Dp_prod+0.3, GR);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = 9
        set(pl(v), 'FaceColor', [0 0 0.6]); % [.9 1 .9]
    end

    xt=[2:1:10 20]; 
    set(gca,'xtick',xt); 
    New_XTickLabel=get(gca,'xtick'); 
    set(gca,'XTickLabel',New_XTickLabel);
    
    xlim([1.3 parms.Dpmax-1]);
    ylim([0 parms.GRmax]);
    setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    c = {'10^{-8}','10^{-7}','10^{-6}','10^{-5}','10^{-4}','10^{-3}','10^{-2}','','Dimers'};
    order = [9 7 6 5 4 3 2 1];
    legend(pl(order), c{order});
    saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');  
    
    %% Stacked plot, Cs vs Dp
    % here assigns log C* = -8 to -5 as ELVOC
    xVarName = 'Dp_prod';
    yVarName = 'Cs_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp_stacked';
    plotCmd = 'area';

    figure
    pl = area(Dp_prod+0.3, Cs_prod);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end

    xlim([1.3 parms.Dpmax]);
    ylim([0 max(sum(Cs_prod,2))]);
    setFigureParameters(paperSize,xLabelStr,'suspended mass (\mu g m^{-3})');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs',verbos,'/Cs_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs',verbos,'/Cs_v_',fileSuffix,'_logx_color.pdf'],'pdf');    
    
    %% Stacked plot, Cs vs t
    % here assigns log C* = -8 to -5 as ELVOC
    xVarName = 't_prod';
    yVarName = 'Cs_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 't_stacked';
    plotCmd = 'area';

    figure
    pl = area(t_prod+0.3, Cs_prod);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end

    %xlim([1.3 parms.Dpmax]);
    %ylim([0 max(sum(Cs_prod,2))]);
    setFigureParameters(paperSize,xLabelStr,'suspended mass (\mu g m^{-3})');
    %set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs',verbos,'/Cs_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs',verbos,'/Cs_v_',fileSuffix,'_logx_color.pdf'],'pdf');  
end

if parms.plotVerbosity >= 2
    %% Condensation sink plot vs time
    xVarName = 't_prod';
    yVarName = 'kc_prod';
    plotCmd = 'plot';
    plotVBSVec;
    setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
    saveas(gcf,['figs',verbos,'/condSink_v_t' '.pdf'],'pdf');
    
    %% Plot condensation flux vs time
    yVarName = 'C_flux';
    plotCmd = 'semilogx';
    plotVBSVec;
    pl = eval([plotCmd,'(',xVarName,'(plotSel),C_flux_tot(plotSel),''k--'')']);
    set(pl,'LineWidth',1.5);
    xlim([1 30]);
    ylim([0 100]);
    setFigureParameters(paperSize,xLabelStr,'Flux per area ');
    saveas(gcf,['./figs/flux_v_',fileSuffix,'_logx.pdf'],'pdf');    
    
    %% Frequency plot vs time
    xVarName = 't_prod';
    yVarName = 'freqA';
    plotVBSVec;
    setFigureParameters(paperSize,'time (min)','Collision frequency per A/V (1/min-m)');
    saveas(gcf,['./figs',verbos,'/freq_v_t.pdf'],'pdf');
    
end

if parms.plotVerbosity >= 3

    %% diagnostic plots, vs time
    xVarName = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't';
    plotCmd = 'semilogy';
    diagnosticPlots;
    
    %% diagnostic plots, vs Dp
    xVarName = 'Dp_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp';
    plotCmd = 'semilogy';
    diagnosticPlots;
end