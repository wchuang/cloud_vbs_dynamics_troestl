function [ dCdt] = dynVBS( t, Ccomb )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

global parms

physicalConstants;

% Ccomb is the combined C column vector
% This is a 9-bin VBS with whatever C*s we want
Cv = Ccomb(1:9);    % vapor organic concentration
Cs = Ccomb(10:18);  % suspended (particle) organic concentration
Cseed = Ccomb(19);  % seed mass concentration
Np = Ccomb(20);     % number concentration

% calculate total suspended OA and then activities (as)
COA = sum(Cs);
Cpart = COA + Cseed*parms.interactSeed;
if Cpart > 0, as = Cs / Cpart; else as = 0; end

% calc production flux in vapor in ug/m3-min
Pv = parms.y * (Lprec(t) + parms.precTimeCoeff * t); %

% now calculate net condensation flux
[kc, Dp] = condSinkSimple(Np,COA,Cseed);
phic_p  = kc' .* (Cv - as .* parms.Co .* 10.^(parms.DK/Dp));

% The differentials, including wall losses

kw = parms.kw;  % the wall loss. Can be a vector or a constant

%% oligomerization function - added 2016/09/05
% we draw parameters from Trump and Donahue 2014, ACP. 
switch parms.oligomerization
    case 1
        %k_reverse = .4; % at 300K, [hr-1]
        %Keq = 1000; % assumed, need Keq > 1 to allow for formation of dimers [unitless]
        k_forward = 20000; %Keq * k_reverse / 60; % [min-1]
        % for logC*=0 monomers, monomer-monomer dimerization cannot occur (in the particle phase)--
            % there is simply too little concentration to allow that. 
            % instead we look at dimerization of logC*=0 monomers with (E)LVOCs
            % if that is the case, then the reaction is P_olig = k_f * massFraction(HOMs) * massFraction(monomer) * COA
            % meaning that oligomerization occurs faster with more mass.
            % if SVOC is small, then massFraction(HOMs)=1, so the equation comes down to P_olig = k_f * massFraction(monomer)*COA
        dimerCreateRate = k_forward * Cs(end); % only for monodisperse
        dimerRateArray = [dimerCreateRate;0;0;0;0;0;0;0;-dimerCreateRate]; % Currently will only work for monodisperse
    case 0
        dimerRateArray = 0;
end

% Time units are in minutes!
dCvdt = Pv - phic_p - kw .* Cv - parms.kflow * Cv;
dCsdt = phic_p - parms.kflow * Cs + dimerRateArray;

dCseeddt = parms.kflow * Cseed * 0;
dNpdt = parms.kflow * Np * 0;

% repack the derivative, neither seed mass nor particle number changes yet
dCdt = [dCvdt; dCsdt; dCseeddt; dNpdt];

end

