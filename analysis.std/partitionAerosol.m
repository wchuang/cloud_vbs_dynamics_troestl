function [partCoeffVec,COAVec] = partitionAerosol(CsVec,CStar)

% Cs is an array, each row is a different time

% Here we need to figure out whether the whole system is above saturation
% or not -- for now I assume that it is.

% because this is iterative we are going row by row
rows = size(CsVec,1);
CStar = CStar';

partCoeffVec = CsVec*0;
COAVec = sum(partCoeffVec,2);

for r = (1: rows)
    
Cs = CsVec(r,:);

sumConc = cumsum(Cs); % add along the rows
xiGuess = sumConc ./ CStar - 1;
xiGuess = min(xiGuess,1);
xiGuess = max(xiGuess,0);
xi = xiGuess;

% Determine the partitioning coefficient
% This is not quite right, but if all the xiGuess terms are 0, unsaturated
err = 1;
maxErr = 1e-4;
COA = 0;
if ((sum(Cs) == 0) || (sum(xiGuess) == 0))
    xi = xi*0;
    err = 0;
end
while err > maxErr
  CsOA = xi .* Cs;
  COA = sum(CsOA);    
  xiNew = 1 ./ (CStar ./ COA + 1);
  err = max(abs(log(xi ./ xiNew)));
  xi = xiNew;
end

partCoeff = xi;

partCoeffVec(r,:) = partCoeff;
COAVec(r) = COA;

end