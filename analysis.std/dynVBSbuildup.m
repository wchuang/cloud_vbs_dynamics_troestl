function [ dCdt] = dynVBSbuildup( t, Ccomb )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

global parms

physicalConstants;

% Ccomb is the combined C column vector
% This is a 9-bin VBS with whatever C*s we want
Cv = Ccomb(1:9);    % vapor organic concentration
Cs = Ccomb(10:18);  % suspended (particle) organic concentration
Cseed = Ccomb(19);  % seed mass concentration
Np = Ccomb(20);     % number concentration

% step 1 calculate total suspended OA and then activities (as)
COA = sum(Cs);
if COA > 0, as = Cs / COA; else as = 0; end

% step 3 calc production flux in vapor in ug/m3-min
Pv = parms.y * Lprec(t); %* (exp(t/250)-1); %* 1/(1+exp(-t/200));
   
% calculate vapor saturation ratio but we don't actually need this...
% Sv = Cv ./ parms.Co;

% now calculate net condensation flux
%{
[kc, Dp] = condSinkSimple(Np,COA,Cseed);
phic_p  = kc .* (Cv - as .* parms.Co);% .* 10.^(parms.DK/Dp));
%}

% The differentials, including wall losses

kw = parms.kw;  % the wall loss. Can be a vector or a constant

% For reasons I don't quite understand I'm using minutes as my time units.
dCvdt = Pv - kw .* Cv - parms.kflow * Cv;
dCsdt = 0 * Cs;

% repack the derivitive, neither seed mass nor particle number changes yet
dCdt = [dCvdt; dCsdt; 0; 0];

end

