% Note that the calculated diameters will be plotted next
% We also recover the condensation frequency per unit particle area
% This shows the effects of reduced mass and diffusion limitations

global parms

[kc_prod, Dp_prod, freqA] = ...
    condSinkSimple(Np_prod,COA_prod,Cseed_prod);

% Getting excess saturation ratio for each bin
% The effective COA includes a contribution from the seeds

Cpart = sum(Cs_prod,2) + Cseed_prod*parms.interactSeed;

% The condensed phase mass fraction as is the suspended concentration (Cs) 
% for each species divided by the total suspended concentration (COA);
as = bsxfun(@rdivide, Cs_prod, Cpart);
as(isnan(as)) = 0 ;

% The Kelvin term
Kelvin = 10.^(parms.DK ./ Dp_prod);

% The condensed phase activity includes the Kelvin term
as_prime = bsxfun(@times, as, Kelvin);

% The equilibrium concentration over a flat mixture
Ceq_flat = bsxfun(@times,as,parms.Co');

% And finally the equilibrium concentration over the particles
Ceq = bsxfun(@times,Ceq_flat,Kelvin);

% The gas-phase saturation ratio is the vapor concentration Cv divided by 
% the saturation mass concentration Co
Sv = bsxfun(@rdivide, Cv_prod, parms.Co');

% Now we can calculate the driving terms.  There are four total terms:
%  1. The saturation difference vs a flat surface (XS_sat_flat)
%  2. The saturation difference over the curved particles
%  3. The absolute difference vs a flat surface
%  4. The absolute difference

XS_sat_flat = Sv - as;
XS_sat = Sv - as_prime;

C_drive_flat = Cv_prod - Ceq_flat;
C_drive = Cv_prod - Ceq;

C_flux = bsxfun(@times,freqA,C_drive);
C_flux_tot = sum(C_flux,2);

% Calculate the number concentration
% The 1e12 is converts the u and the m3 in ug/m3 to cm-3
% Take only the columns that we call HOMs with the selector
Nv = bsxfun(@rdivide,Cv_prod,repmat(parms.Morg',size(Cv_prod,1),1));
Nv = Nv * avogadroConstant / 1e12;
Nv = Nv(:,parms.selHOM);
Nv_tot = sum(Nv,2);

% The maximum flux is the flux considering vapor concentration only
% We apply a selector for the HOMs in the basis set
% Take only the columns that we call HOMs with the selector
C_flux_max = bsxfun(@times,freqA,Cv_prod);
C_flux_max = C_flux_max(:,parms.selHOM);
C_flux_max_tot = sum(C_flux_max,2);

% the fluxes are in 1/min and the GR is nm h-1 so mult by 60
GRmult = 2 * 60 / parms.rhoOrg;
GR = bsxfun(@times,C_flux,GRmult);
GR_tot = C_flux_tot * GRmult;
GR_tot_max = C_flux_max_tot * GRmult;

% make a selector for the plots
plotSel = Dp_prod > 0.75;