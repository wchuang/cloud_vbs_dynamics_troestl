% script plotVBSVec
% repeated figure call with xVarName and yVarName
figure
fStr = [plotCmd,'(',xVarName,'(plotSel),',yVarName,'(plotSel,1),''g-'')'];
pl = eval(fStr);
set(pl,'Color',[0,1/10,0]);
set(pl,'LineWidth',1.5);
hold
for v = (2:eval(['size(',yVarName,',2)']))
    pl = eval([plotCmd,'(',xVarName,'(plotSel),',yVarName,'(plotSel,v),''g-'')']);
    set(pl,'Color',[0,v/10,0]);
    set(pl,'LineWidth',1.5);
end
