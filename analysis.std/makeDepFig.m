function makeDepFig

figure

Dp = (.75:.01:50);
Dp_points = [1.7,10];

[sip, sGR] = depSpeed(Dp,350,1.4e3,.92);

[sip_points, sGR_points] = depSpeed(Dp_points,350,1.4e3,.92)


pl = semilogx(Dp,sip,'g-');
set(pl,'linewidth',2);
ylim([0,500])
xlim([1,50])

setFigureParameters([6.5,3],'D_p (nm)','s_{i,p} (m s^{-1})');

saveas(gcf,'./figs/depositionSpeed.pdf','pdf');

pl = semilogx(Dp,sGR,'g-');
set(pl,'linewidth',2);
hold;
pl = semilogx(Dp_points,sGR_points,'go');
set(pl,'MarkerFaceColor','g');
set(pl,'MarkerSize',15);

ylim([0,600])
xlim([1,50])

setFigureParameters([6.5,3],'D_p (nm)','s^{GR}_{i,p} (nm h^{-1}/ \mu g m^{-3})');

saveas(gcf,'./figs/depositionGR.pdf','pdf');
