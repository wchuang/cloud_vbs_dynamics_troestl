function  [t_prod Cprod] = simpleVBSDynamics()
%simpleVBSDynamics do simple dynamical calculations of condensation
%   Detailed explanation goes here

set(0,'DefaultFigureWindowStyle','docked')

global parms

paperSize = [9 5];

physicalConstants;

Crun(1:9) = 0; % initialize vapor concentrations

%% vapor buildup, only if startTime < 0
if parms.timeStart < 0
    vaporBuildup;
    Crun = Cinit(end,:)'; % set the dynamic run to vapor concentrations from buildup
    %tSpan = (.05:.05:parms.timeEnd);
    tSpan = (.05:.05:parms.timeEnd);
else
    %tSpan = (parms.timeStart:.05:parms.timeEnd);
    tSpan = (parms.timeStart:.05:parms.timeEnd);
end

%% Condensation step

Cseed = 1/6*pi*(parms.DpSeed*nm2m)^3 * (parms.Np*cm3tom3) * parms.rhoSeed * kgm3tougm3;
Crun(10:18) = 0;
Crun(19) = Cseed;   % suspended seed mass during production
Crun(20) = parms.Np;      % suspended particle number during production

options = odeset('NonNegative',ones(size(Crun)),'RelTol',parms.relTol,'AbsTol',parms.absTol);
[t_prod Cprod] = ode15s(@dynVBS,tSpan,Crun,options);

Cv_prod = Cprod(:,(1:9));     % vapors during dilution
Cs_prod = Cprod(:,(10:18));   % suspended particles during dilution
Cseed_prod = Cprod(:,19);     % suspended seed mass during dilution
Np_prod = Cprod(:,20);        % suspended particle number during dilution

COA_prod = sum(Cs_prod,2);    % sum of each row
Cvap_prod = sum(Cv_prod,2);   % sum of each row for vapors


%C_vapELVOCprod = sum(bsxfun(@times,Cv_prod,parms.chargeEfficiency'),2); % sum of all ELVOCs
C_vapELVOCmlcprod = Cv_prod./350*6e23/1e12; % is called in vaporPlot.m

%vaporPlot;
%particlePlot;
%totalPlot;

% make a bunch of other plots
mainPlots;
%summaryPlots;

end

