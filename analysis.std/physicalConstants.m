% Gas constant
R = 8.314472;
avogadroConstant = 6.022e23;

% Conversions
nm2m = 1e-9;                % conversion
kgm3tougm3 = 1e9;           % conversion
cm3tom3 = 1e6;              % conversion                    
sec2min = 60;               % conversion
g2kg = 1e-3;

amutokg = 1.660538921e-27;
ugtoamu = 1e-9/amutokg;

