% This file calculates the buildup of first generation productsin the
% chamber prior to the introduction of seeds and the start of condensation.
% Vapor wall loss is the only loss factors here.

global parms

Np = 0;
Cv = zeros(9,1);             % vapor concentration
Cs = zeros(9,1);             % suspended org concentration

% Calculate seed concentration
Cseed = 1/6*pi*(parms.DpSeed*nm2m)^3 * (parms.Np*cm3tom3) * parms.rhoSeed * kgm3tougm3;

% Building constant concentration

tSpan = (parms.timeStart:.05:0); % in minutes

% We need to build up a single y column vector for the ode solver
% This is [vapor; suspended; seed mass conc; seed number concentration] for now
Ccomb = [Cv; Cs; Cseed; Np];

% Now we feed the combined initial condition vector to a differential
% equation solver and get back the solution vectors
% We are going to label these init for "buildup" phase
[t_init Cinit] = ode23s(@dynVBSbuildup,tSpan,Ccomb);

% Once the solution is done we can unwrap...
Cv_init = Cinit(:,(1:9));   % vapors during production
Cs_init = Cinit(:,(10:18)); % suspended particles during production
Cseed_init = Cinit(:,19);   % suspended seed mass during production
Np_init = Cinit(:,20);      % suspended particle number during production

COA_init = sum(Cs_init,2);  % sum of each row for suspended particles
Cvap_init = sum(Cv_init,2); % sum of each row for vapors

C_vapELVOC = sum(bsxfun(@times,Cv_init,parms.chargeEfficiency'),2); % sum of all ELVOCs

% HOM number conc to be graphed in vaporPlot.m
C_vapELVOCmlcinit = C_vapELVOC/250*6e23/1e12;