function [ output_args ] = crudeVBSEuler( input_args )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

tm1 = 0;
for t = [(.0001:.0001:0.01) (0.02:0.01:100)]

% timestep in min...
dt = t-tm1;

dCdt = dynVBS(t, [Cv; Cs]);

dCvdt = dCdt(1:4);
dCsdt = dCdt(5:8);

dCv = dCvdt * dt;
dCs = dCsdt * dt;


Cv = Cv + dCv;
Cs = Cs + dCs;

Cfill = (Cs < 0) .* Cs;

Cv = Cv + Cfill
Cs = Cs - Cfill

% tm1 is the previous time
tm1 = t;


end

Ctot = Cv + Cs

end

